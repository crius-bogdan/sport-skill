// window.addEventListener('DOM')
var doc = document,
    menuWrap = doc.querySelector('.navigation-ac'),
    burgerBtn = doc.querySelector('.menu-mob'),
    supportBtn = doc.querySelector('.support__btn'),
    supportWrapItem = doc.querySelector('.support__items-wrap'),
    message = doc.querySelector('.message__text'),
    messWrap = doc.querySelector('.message'),
    wrapDporZone = doc.querySelector('.user-image__upload'),
    overlay = doc.querySelector('.overlay'),
    modal = doc.querySelector('.ad-modal'),
    num,dataId;
//  $(').on('click', function (e) {
//      e.preventDefault();
//      $(this).toggleClass('menu-btn_active');
//      $('.support__items-wrap').toggleClass('menu_active');
//  });
document.addEventListener('click', function(event) {
    var target = event.target;
    
    while(target != this) {
        if(target.classList.contains('menu-mob__btn')) {
            if(!burgerBtn.classList.contains('arrow')) {
                burgerBtn.classList.add('arrow');
            } else {
                burgerBtn.classList.remove('arrow');
            }
            if (!menuWrap.classList.contains('open')) {
                menuWrap.classList.add('open');
            } else {
                menuWrap.classList.remove('open');
            }
        }
        if (target.classList.contains('support__btn')) {
            event.preventDefault();
            if (!supportBtn.classList.contains('support__btn_active')) {
                supportBtn.classList.add('support__btn_active');
            } else {
                supportBtn.classList.remove('support__btn_active');
            }
            if (!supportWrapItem.classList.contains('support__items-wrap_active')) {
                supportWrapItem.classList.add('support__items-wrap_active');
            } else {
                supportWrapItem.classList.remove('support__items-wrap_active');
            }
        }
        if (target.classList.contains('user-image__link-photo')) {
            event.preventDefault();
            wrapDporZone.classList.add('active');
        }
        if (target.classList.contains('admin-users__btn') || target.classList.contains('admin-users__link-settings')) {
            num = target.getAttribute('data-num');
            if (num != null) {
                event.preventDefault();
                overlay.classList.add('show');
                dataId = target.getAttribute('data-id');
                document.querySelector('.hidden-'+ num).value = dataId;
                modal.classList.add('ad-modal_active-'+ num);
            } else {
                return;
            }
        }
        if (target.classList.contains('overlay') || target.classList.contains('ad-modal__save')) {
            if (target.classList.contains('ad-modal__save-no')) {
                event.preventDefault();
            }
            overlay.classList.remove('show');
            modal.classList.remove('ad-modal_active-' + num);
                
        }
        target = target.parentNode;
    }
});
$('.faq-content__item').click(function (event) {
    $(this).toggleClass('open')
    $(this).children(".faq-content__quest_content").slideToggle();
    event.stopPropagation();
});
var lineChartData = {
    labels: [
        '1 Неделя',
        '2 Неделя',
        '3 Неделя',
        '4 Неделя',
        '5 Неделя',
        '6 Неделя',
        '7 Неделя',
        '8 Неделя',
        '9 Неделя',
        '10 Неделя',
        '11 Неделя',
        '12 Неделя',
        '13 Неделя',
        '14 Неделя'
    ],
    datasets: [{
        label: 'Бег',
        borderColor: '#FF6C40',
        backgroundColor: '#FF6C40',
        fill: false,
        data: [
            25, 20, 12, 11, 2, 6, 7
        ],
        yAxisID: 'y-axis-1',
    }, {
        label: 'Велосипед',
        borderColor: '#826AF9',
        backgroundColor: '#826AF9',
        fill: false,
        data: [
            1, 12, 25, 18, 8, 10, 11
        ],
        yAxisID: 'y-axis-1'
    }, {
        label: 'Плаванье',
        borderColor: '#FFE700',
        backgroundColor: '#FFE700',
        fill: false,
        data: [
            10, 15, 22, 5
        ],
        yAxisID: 'y-axis-1'
    }]
};
var myDropzone = new Dropzone(".user-image__drop-zone",
    {
        url: "/file/post",
        maxFiles: 1,
        thumbnailWidth: 150,
        thumbnailHeight: 150,
        thumbnailWidth: 150,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictRemoveFile: 'Удалить'

    }
);
// myDropzone.on("addedfile", function(file) { 
//     messWrap.classList.add('active');
//     message.textContent = 'Фото добавлено';
//      setTimeout(function(){
//      messWrap.classList.remove('active');
//      }, 5000);
// });
myDropzone.on('removedfile', function (file) {
    messWrap.classList.add('active');
    message.textContent = 'Фото удалено';
    setTimeout(function () {
        messWrap.classList.remove('active');
    }, 3000);
});
myDropzone.on('error', function (file) {
    messWrap.classList.add('active');
    message.textContent = 'Произошла ошибка при загрузке фото, попробуйте еще раз';

    setTimeout(function () {
        messWrap.classList.remove('active');
    }, 3000);
});
myDropzone.on('success', function (file) {
    messWrap.classList.add('active');
    message.textContent = 'Фото успешно загружено!';
    setTimeout(function () {
        wrapDporZone.classList.remove('active');
    }, 2000);
    setTimeout(function () {
        mesWrap.classList.remove('active');
    }, 3000);
});
myDropzone.on("maxfilesexceeded", function (file) { this.removeFile(file); });
window.onload = function () {
    var ctx = document.getElementById('charts').getContext('2d');
    console.log(Chart.defaults.global.tooltips);
    window.myLine = Chart.Line(ctx, {
        data: lineChartData,
        options: {
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            tooltips: {
                backgroundColor: "#fff",
                bodyFontColor: "#000",
                // borderColor: "rgba(0, 0, 0, 0.12)",
                // borderWidth: 1,
                bodyFontSize: 16,
                bodyFontStyle: 'bold',
                caretPadding: 8,
                caretSize: 5,
                cornerRadius: 4,
                displayColors: false,
                enabled: true,
                footerFontColor: "#000",
                footerFontStyle: "bold",
                footerMarginTop: 6,
                titleFontColor: "#000",
                titleFontStyle: "bold",
                titleSpacing: 2,
                xPadding: 8,
                yPadding: 8,
                callbacks: {
                    label: function (tooltipItem, data) {
                        // console.log(data.datasets[tooltipItem.datasetIndex].label);
                        var label = data.datasets[tooltipItem.datasetIndex].label;
                        var r = /\d+/;

                        if (label) {
                            label += ': ';
                        }
                        label += Math.round(tooltipItem.yLabel * 100) / 100;
                        console.log(label + 'km')
                        return label.match(r) + 'км';
                    },
                    afterLabel: function (data) {

                    },
                    title: function (data, label) {


                    }
                }
            },
            elements: {
                line: {
                    borderWidth: 4,

                },
                point: {
                    hoverRadius: 8,
                    radius: 5,
                    // pointStyle: 'star'
                }
            },
            title: {
                display: false
                // text: 'Chart.js Line Chart - Multi Axis'
            },
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: 'left',
                    id: 'y-axis-1',
                }],
            }
        }
    });
};