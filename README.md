Для того чтобы заработали модели требется,
в конец тега 
<header>
    <div  id="model"></div>
    вставить такой кусок кода 
</header>

Можно вставить этот скрипт в helpful.js в конец.
(function(){
    var regTanks = /tanks/,
    regWarcraft = /warcraft/,
    regPatchof = /patchofexile/,
    regLegend = /legend/,
    regHeroes = /heroes/,
    regDota = /dota/,
    regDestiny = /destiny/,
    pageLocation = window.location.pathname;

    if(regTanks.test(pageLocation)) {
        // url for model tank
        loadModel('models/tank/scene.gltf', {
            directionalLight: 0.3
        });

    } else if (regWarcraft.test(pageLocation)) {
        // url for model Warcraft
        loadModel('models/wow/scene.gltf', {
            scale: .3,
            z: -10,
            light: 2,
            camera: {x: 0, y: 0, z: 10}
        });
    } else if (regPatchof.test(pageLocation)) {
        // url for model Patch
        loadModel('models/patch/scene.gltf', {
            scale: .1,
            z: -14,
            camera: {x: 0, y: 1, z: 10}
        });

    } else if (regLegend.test(pageLocation)) {
        // url for model Legend
        loadModel('models/legend/scene.gltf', {
            scale: .06,
            z: -10,
            y: -1,
            ambientLight: 0,
            camera: {x: 2, y: 0, z: 20},
            control: {y: 1}
        });

    } else if (regHeroes.test(pageLocation)) { 
        // url for model Heroes
        loadModel('models/heroes/scene.gltf', {
            scale: .2,
            z: -14,
            directionalLight: 0.01,
            camera: {x: 0, y: 0, z: 10}
        });

    } else if (regDota.test(pageLocation)) {
        // url for model Dota
        loadModel('models/dota2/scene.gltf', {
            scale: .017,
            z: -10,
            x: 2,
            y: -1,
            camera: {x: 2, y: 13, z: 13},
            control: {y: 1}
        });

    } else if (regDestiny.test(pageLocation)) { 
        // url for model Destiny
        loadModel('models/destiny2/scene.gltf', {
            scale: .04,
            z: -24,
            x: 1,
            zoom: 25
        });
    }
}());